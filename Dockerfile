FROM node

WORKDIR /opt/app
COPY package*.json ./
COPY . .
EXPOSE 3000
CMD [ "node", "server.js" ]